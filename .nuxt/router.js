import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _4faa0da4 = () => interopDefault(import('..\\pages\\about.vue' /* webpackChunkName: "pages/about" */))
const _6c9f42dc = () => interopDefault(import('..\\pages\\admin\\index.vue' /* webpackChunkName: "pages/admin/index" */))
const _1240f676 = () => interopDefault(import('..\\pages\\cardlist\\index.vue' /* webpackChunkName: "pages/cardlist/index" */))
const _b3db7118 = () => interopDefault(import('..\\pages\\home\\index.vue' /* webpackChunkName: "pages/home/index" */))
const _7ceb11ec = () => interopDefault(import('..\\pages\\login.vue' /* webpackChunkName: "pages/login" */))
const _e213a8e4 = () => interopDefault(import('..\\pages\\posts\\index.vue' /* webpackChunkName: "pages/posts/index" */))
const _f81499ec = () => interopDefault(import('..\\pages\\profile.vue' /* webpackChunkName: "pages/profile" */))
const _4ea3cd92 = () => interopDefault(import('..\\pages\\register.vue' /* webpackChunkName: "pages/register" */))
const _610272e4 = () => interopDefault(import('..\\pages\\todos.vue' /* webpackChunkName: "pages/todos" */))
const _27161639 = () => interopDefault(import('..\\pages\\users\\index.vue' /* webpackChunkName: "pages/users/index" */))
const _7394400f = () => interopDefault(import('..\\pages\\admin\\posts\\index.vue' /* webpackChunkName: "pages/admin/posts/index" */))
const _7972c11a = () => interopDefault(import('..\\pages\\cardlist\\Cerberus.vue' /* webpackChunkName: "pages/cardlist/Cerberus" */))
const _136fe676 = () => interopDefault(import('..\\pages\\cardlist\\Endymion.vue' /* webpackChunkName: "pages/cardlist/Endymion" */))
const _26a80c8f = () => interopDefault(import('..\\pages\\admin\\posts\\create.vue' /* webpackChunkName: "pages/admin/posts/create" */))
const _79379afe = () => interopDefault(import('..\\pages\\users\\_id\\index.vue' /* webpackChunkName: "pages/users/_id/index" */))
const _103a581a = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _4faa0da4,
    name: "about"
  }, {
    path: "/admin",
    component: _6c9f42dc,
    name: "admin"
  }, {
    path: "/cardlist",
    component: _1240f676,
    name: "cardlist"
  }, {
    path: "/home",
    component: _b3db7118,
    name: "home"
  }, {
    path: "/login",
    component: _7ceb11ec,
    name: "login"
  }, {
    path: "/posts",
    component: _e213a8e4,
    name: "posts"
  }, {
    path: "/profile",
    component: _f81499ec,
    name: "profile"
  }, {
    path: "/register",
    component: _4ea3cd92,
    name: "register"
  }, {
    path: "/todos",
    component: _610272e4,
    name: "todos"
  }, {
    path: "/users",
    component: _27161639,
    name: "users"
  }, {
    path: "/admin/posts",
    component: _7394400f,
    name: "admin-posts"
  }, {
    path: "/cardlist/Cerberus",
    component: _7972c11a,
    name: "cardlist-Cerberus"
  }, {
    path: "/cardlist/Endymion",
    component: _136fe676,
    name: "cardlist-Endymion"
  }, {
    path: "/admin/posts/create",
    component: _26a80c8f,
    name: "admin-posts-create"
  }, {
    path: "/users/:id",
    component: _79379afe,
    name: "users-id"
  }, {
    path: "/",
    component: _103a581a,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
