# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Homepage>` | `<homepage>` (components/homepage.vue)
- `<Shoppingcart>` | `<shoppingcart>` (components/shoppingcart.vue)
- `<UserAuthForm>` | `<user-auth-form>` (components/UserAuthForm.vue)
- `<PostsHeader>` | `<posts-header>` (components/posts/Header.vue)
- `<PostsHeadtopdeck>` | `<posts-headtopdeck>` (components/posts/headtopdeck.vue)
- `<PostsPostContent>` | `<posts-post-content>` (components/posts/PostContent.vue)
- `<PostsPostList>` | `<posts-post-list>` (components/posts/postList.vue)
- `<PostsPostcardlist>` | `<posts-postcardlist>` (components/posts/postcardlist/postcardlist.vue)
- `<PostsPostcardlistShowcardlist>` | `<posts-postcardlist-showcardlist>` (components/posts/postcardlist/showcardlist.vue)
