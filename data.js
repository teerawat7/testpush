products = [
    {
      id: 1,
      titlecard: "Endymion",
      imagecard: "https://ygoprodeck.com/pics/3611830.jpg",
      pricecard: 100,
    },
    {
      id: 2,
      titlecard: "Mythical",
      imagecard: "https://s3.duellinksmeta.com/cards/60c2b3aba0e24f2d54a52d7d_w420.webp",
      pricecard: 200,
    },
    {
      id: 3,
      titlecard: "Astrograph Sorcerer",
      imagecard: "https://s3.duellinksmeta.com/cards/60c2b3a9a0e24f2d54a515cc_w420.webp",
      pricecard: 300,
    },
  ];
  
  module.exports = function () {
    return {
        products: products,
    };
  };
  